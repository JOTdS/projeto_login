<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'O :attribute deve ser aceito.',
    'active_url' => 'O :attribute não é um URL válido.',
    'after' => ' :attribute deve ser uma data depois :date.',
    'after_or_equal' => ' :attribute deve ser uma data depois ou igual :date.',
    'alpha' => 'The :attribute pode conter apenas letras.',
    'alpha_dash' => 'The :attribute pode conter apenas letras, números, traços e sublinhados.',
    'alpha_num' => 'Números :attribute pode conter apenas letras e números.',
    'array' => 'Números :attribute devem ser an array.',
    'before' => ' :attribute deve ser uma data antes :date.',
    'before_or_equal' => ' :attribute deve ser uma data antes ou igual :date.',
    'between' => [
        'numeric' => ' :attribute deve estar entre :min e :max.',
        'file' => ' :attribute deve estar entre :min e :max kilobytes.',
        'string' => ':attribute deve estar entre :min e :max caracteres.',
        'array' => ' :attribute deve estar entre :min e :max itens.',
    ],
    'boolean' => ' :attribute campo deve ser verdadeiro ou falso.',
    'confirmed' => ' :attribute não é igual.',
    'date' => ' :attribute não é uma data válida.',
    'date_equals' => ' :attribute deve ser uma data igual a :date.',
    'date_format' => ' :attribute não corresponde ao formato :format.',
    'different' => ' :attribute e :other deve ser diferente.',
    'digits' => ' :attribute devem ser :digits digitos.',
    'digits_between' => ' :attribute deve estar entre :min e :max digitos.',
    'dimensions' => ' :attribute tem dimensões de imagem inválidas.',
    'distinct' => ' :attribute campo tem um valor duplicado.',
    'email' => ' :attribute Deve ser um endereço de e-mail válido.',
    'ends_with' => ' :attribute deve terminar com um dos seguintes: :values.',
    'exists' => '  :attribute selecionado é inválido.',
    'file' => ' :attribute deve ser um arquivo.',
    'filled' => ' :attribute campo deve ter um valor.',
    'gt' => [
        'numeric' => ' :attribute deve ser maior que :value.',
        'file' => ' :attribute deve ser maior que :value kilobytes.',
        'string' => ' :attribute deve ser maior que :value caracteres.',
        'array' => ' :attribute must have more que :value itens.',
    ],
    'gte' => [
        'numeric' => ' :attribute deve ser maior ou igual :value.',
        'file' => ' :attribute deve ser maior ou igual :value kilobytes.',
        'string' => ' :attribute deve ser maior ou igual :value caracteres.',
        'array' => ' :attribute deve ter :value itens ou mais.',
    ],
    'image' => ' :attribute deve ser uma imagem.',
    'in' => ' selected :attribute é invalido.',
    'in_array' => ' :attribute campo não existe em :other.',
    'integer' => ' :attribute deve ser um número inteiro.',
    'ip' => ' :attribute deve ser um válido IP endereço.',
    'ipv4' => ' :attribute deve ser um válido IPv4 endereço.',
    'ipv6' => ' :attribute deve ser um válido IPv6 endereço.',
    'json' => ' :attribute deve ser um válido JSON string.',
    'lt' => [
        'numeric' => ' :attribute deve ser menor que :value.',
        'file' => ' :attribute deve ser menor que :value kilobytes.',
        'string' => ' :attribute deve ser menor que :value caracteres.',
        'array' => ' :attribute deve ter menos de :value itens.',
    ],
    'lte' => [
        'numeric' => ' :attribute deve ser menor ou igual :value.',
        'file' => ' :attribute deve ser menor ou igual :value kilobytes.',
        'string' => ' :attribute deve ser menor ou igual :value caracteres.',
        'array' => ' :attribute must not have more than :value itens.',
    ],
    'max' => [
        'numeric' => ' :attribute não pode ser maior que :max.',
        'file' => ' :attribute não pode ser maior que :max kilobytes.',
        'string' => ' :attribute não pode ser maior que :max caracteres.',
        'array' => ' :attribute may not have more than :max itens.',
    ],
    'mimes' => ' :attribute devem ser a file of type: :values.',
    'mimetypes' => ' :attribute devem ser a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute deve ser um arquivo de :min.',
        'file' => 'The :attribute deve ser um arquivo de :min kilobytes.',
        'string' => 'The :attribute deve ser um arquivo de :min caracteres.',
        'array' => 'The :attribute deve ter pelo menos :min itens.',
    ],
    'not_in' => ' :attribute selecionado é invalido.',
    'not_regex' => ' :attribute o formato é invalido.',
    'numeric' => ' :attribute deve ser um número.',
    'password' => ' senha incorreta.',
    'present' => ' :attribute campo deve estar presente.',
    'regex' => ' :attribute o formato é invalido.',
    'required' => ' :attribute o campo é obrigatório.',
    'required_if' => ' :attribute campo é obrigatório quando :other é :value.',
    'required_unless' => ' :attribute campo é obrigatório a menos que :other estiver em :values.',
    'required_with' => ' :attribute campo é obrigatório quando :values está presente.',
    'required_with_all' => ' :attribute campo é obrigatório quando :values é recente.',
    'required_without' => ' :attribute campo é obrigatório quando :values não está presente.',
    'required_without_all' => ' :attribute campo é obrigatório quando nenhum dos :values estão presentes.',
    'same' => ' :attribute e :other deve combinar.',
    'size' => [
        'numeric' => ' :attribute devem ser :size.',
        'file' => ' :attribute devem ser :size kilobytes.',
        'string' => ' :attribute devem ser :size caracteres.',
        'array' => ' :attribute deve conter :size itens.',
    ],
    'starts_with' => ' :attribute deve começar com um dos seguintes: :values.',
    'string' => ' :attribute devem ser uma string.',
    'timezone' => ' :attribute deve ser uma zona valida.',
    'unique' => ' :attribute já foi utilizado.',
    'uploaded' => ' :attribute falha ao carregar.',
    'url' => ' :attribute o formato é invalido.',
    'uuid' => ' :attribute deve ser um válido UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail endereço" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
